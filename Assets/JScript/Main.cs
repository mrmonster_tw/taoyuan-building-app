﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using RenderHeads.Media.AVProVideo;

public class Main : MonoBehaviour
{
    //public Text test;
    public GameObject video_ob;
    public VideoPlayer vido;
    //public VideoClip video_1;
    //public VideoClip video_2;
    //public VideoClip video_3;
    public GameObject video_1;
    public GameObject video_2;
    public GameObject video_3;

    public bool video_1_check;
    public bool video_2_check;
    public bool video_3_check;
    public bool pause_check;
    public bool coutinue_check;
    public bool home_bt;
    public bool building_bt;

    public GameObject bg;
    public GameObject setting;
    public GameObject ip;
    public GameObject rotate_bt;
    public GameObject reset_bt;
    public GameObject pause_bt;
    public GameObject play_bt;
    public GameObject quit_bt;
    public Animator build_ani;
    public InputField ipadress_input;
    public Text ipadress;
    public Material black;

    public NetworkManager manager;

    public int first_check;
    public int once;

    //public GameObject building;

    // Start is called before the first frame update
    void Start()
    {
        manager = GetComponent<NetworkManager>();
        first_check = PlayerPrefs.GetInt("first_check");
        if (first_check == 1)
        {
            manager.networkAddress = PlayerPrefs.GetString("ipadress");
            ipadress_input.text = PlayerPrefs.GetString("ipadress");
            ipadress.text = PlayerPrefs.GetString("ipadress");
        }
        black.color = new Color(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            setting.active = !setting.active;
            ip.active = !ip.active;
            quit_bt.active = !quit_bt.active;
        }
        /*if (vido.isPaused)
        {
            if (black.color.a < 1)
            {
                black.color = new Color(0, 0, 0, black.color.a + 1f * Time.deltaTime);
            }
            if (black.color.a >= 1)
            {
                black.color = new Color(0, 0, 0, 1);
            }
            StartCoroutine(video_wait());
        }*/
    }
    public void video_fn(int xx)
    {
        if (xx == 1)
        {
            video_1_check = true;
        }
        if (xx == 2)
        {
            video_2_check = true;
        }
        if (xx == 3)
        {
            video_3_check = true;
        }
        if (xx == 4)
        {
            pause_check = true;
        }
        if (xx == 5)
        {
            coutinue_check = true;
        }
        if (xx == 6)
        {
            home_bt = true;
        }
        if (xx == 7)
        {
            building_bt = true;
        }
    }
    public void OnApplicationQuit()
    {
        manager.StopHost();
        Application.Quit();
    }
    public void ip_setting()
    {
        manager.networkAddress = ipadress.text;
        first_check = 1;
        PlayerPrefs.SetString("ipadress", ipadress.text);
        PlayerPrefs.SetInt("first_check", first_check);
    }
    IEnumerator video_wait()
    {
        yield return new WaitForSeconds(2);
        vido.clip = null;
        bg.SetActive(true);
        black.color = new Color(0, 0, 0, 0);
    }
}
