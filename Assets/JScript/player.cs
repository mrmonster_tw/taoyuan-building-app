﻿using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class player : NetworkBehaviour
{
    public GameObject main;
    GameObject video_cn;
    public GameObject building;

    [SyncVar]
    public Vector3 scl;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (main == null)
        {
            main = GameObject.FindGameObjectWithTag("main");
        }
        if (building == null)
        {
            building = GameObject.FindGameObjectWithTag("building");
        }
        if (video_cn != null && video_cn.GetComponent<MediaPlayer>().Control.IsFinished())
        {
            Destroy(video_cn);
            video_cn = null;
            main.GetComponent<Main>().bg.SetActive(true);
        }

        if (isServer)
        {
            scl = building.transform.localScale;
        }
        if (isLocalPlayer)
        {
            building.transform.localScale = scl;
            if (main.GetComponent<Main>().video_1_check)
            {
                Rpcopen(1);
            }
            if (main.GetComponent<Main>().video_2_check)
            {
                Rpcopen(2);
            }
            if (main.GetComponent<Main>().video_3_check)
            {
                Rpcopen(3);
            }
            if (main.GetComponent<Main>().pause_check)
            {
                Rpcopen(4);
            }
            if (main.GetComponent<Main>().coutinue_check)
            {
                Rpcopen(5);
            }
            if (main.GetComponent<Main>().home_bt)
            {
                Rpcopen(6);
            }
            if (main.GetComponent<Main>().building_bt)
            {
                Rpcopen(7);
            }
        }
    }
    [ClientRpc]
    void Rpcopen(int yy)
    {
        if (yy == 1)
        {
            video_cn = Instantiate(main.GetComponent<Main>().video_1);
            //main.GetComponent<Main>().vido.clip = null;
            //main.GetComponent<Main>().vido.clip = main.GetComponent<Main>().video_1;
            //main.GetComponent<Main>().vido.Play();
            main.GetComponent<Main>().bg.SetActive(false);
            main.GetComponent<Main>().quit_bt.SetActive(false);
            main.GetComponent<Main>().video_1_check = false;
        }
        if (yy == 2)
        {
            video_cn = Instantiate(main.GetComponent<Main>().video_2);
            //main.GetComponent<Main>().vido.clip = null;
            //main.GetComponent<Main>().vido.clip = main.GetComponent<Main>().video_2;
            //main.GetComponent<Main>().vido.Play();
            main.GetComponent<Main>().bg.SetActive(false);
            main.GetComponent<Main>().quit_bt.SetActive(false);
            main.GetComponent<Main>().video_2_check = false;
        }
        if (yy == 3)
        {
            video_cn = Instantiate(main.GetComponent<Main>().video_3);
            //main.GetComponent<Main>().vido.clip = null;
            //main.GetComponent<Main>().vido.clip = main.GetComponent<Main>().video_3;
            //main.GetComponent<Main>().vido.Play();
            main.GetComponent<Main>().bg.SetActive(false);
            main.GetComponent<Main>().quit_bt.SetActive(false);
            main.GetComponent<Main>().video_3_check = false;
        }
        if (yy == 4)
        {
            video_cn.GetComponent<MediaPlayer>().Pause();
            //main.GetComponent<Main>().vido.playbackSpeed = 0;
            main.GetComponent<Main>().pause_check = false;
        }
        if (yy == 5)
        {
            video_cn.GetComponent<MediaPlayer>().Play();
            //main.GetComponent<Main>().vido.playbackSpeed = 1;
            main.GetComponent<Main>().coutinue_check = false;
        }
        if (yy == 6)
        {
            //main.GetComponent<Main>().vido.clip = null;
            main.GetComponent<Main>().bg.SetActive(true);
            main.GetComponent<Main>().black.color = new Color(0, 0, 0, 0);
            //main.GetComponent<Main>().vido.playbackSpeed = 1;
            //main.GetComponent<Main>().video_ob.SetActive(true);
            main.GetComponent<Main>().build_ani.Play("no");
            main.GetComponent<Main>().rotate_bt.SetActive(false);
            main.GetComponent<Main>().reset_bt.SetActive(false);
            main.GetComponent<Main>().pause_bt.SetActive(true);
            main.GetComponent<Main>().play_bt.SetActive(true);
            main.GetComponent<Main>().home_bt = false;
            Destroy(video_cn);
            video_cn = null;
        }
        if (yy == 7)
        {
            main.GetComponent<Main>().build_ani.Play("build");
            main.GetComponent<Main>().bg.SetActive(false);
            //main.GetComponent<Main>().video_ob.SetActive(false);
            main.GetComponent<Main>().rotate_bt.SetActive(true);
            main.GetComponent<Main>().reset_bt.SetActive(true);
            main.GetComponent<Main>().pause_bt.SetActive(false);
            main.GetComponent<Main>().play_bt.SetActive(false);
            main.GetComponent<Main>().building_bt = false;
        }
    }
}
