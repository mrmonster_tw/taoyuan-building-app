﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SCshoose : MonoBehaviour
{
    public NetworkManager manager;
    public bool Sever;
    public bool Client;
    

    // Start is called before the first frame update
    void Start()
    {
        manager = GetComponent<NetworkManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!NetworkClient.active && !NetworkServer.active && manager.matchMaker == null)
        {
            if (Sever)
            {
                manager.StartHost();
            }
            if (Client)
            {
                manager.StartClient();
            }
        }
    }
}
